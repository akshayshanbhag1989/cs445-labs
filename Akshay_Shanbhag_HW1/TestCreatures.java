import java.util.*;

public class TestCreatures {

	public static int THING_COUNT = 8;
	public static int CREATURE_COUNT = 4;

	public static void main(String[] args) {

		Object[] thingArray =  new Object[THING_COUNT];

		Thing thing1 = new Thing("Banana");
		
		thingArray[0] = thing1.getThing_Name();

		Thing thing2 = new Thing("Tigger, Pooh's Friend");

		thingArray[1] = thing2.getThing_Name();

		Thing thing3 = new Thing("Locomotive");

		thingArray[2] = thing3.getThing_Name();

		Thing thing4 = new Thing("Tick-Tock the Crocodile");

		thingArray[3] = thing4.getThing_Name();

		Object[] creatureArray =  new Object[CREATURE_COUNT];

		Thing tiger1 = new Tiger("BengalTiger");
		creatureArray[0] = tiger1.getThing_Name();

		Thing ant1 = new Ant("RedAnt");
		creatureArray[1] = ant1.getThing_Name();

		Thing bat1 = new Tiger("Batman");
		creatureArray[2] = bat1.getThing_Name();

		Thing fly1 = new Tiger("ZenFly");
		creatureArray[3] = fly1.getThing_Name();

		int x = 4;
		for(int index = 0;index < creatureArray.length;index++)
		{
			thingArray[x] =  creatureArray[index];
			x++;
		}
		System.out.println("Things:");
		System.out.println();
		for(int index = 0;index < 4;index++)
		{
			if(thingArray[index] == "Banana")
			{
				System.out.println(thingArray[index] +": I am sweet in taste and yellow or green in color.");
			}
			if(thingArray[index] == "Tigger, Pooh's Friend")
			{
				System.out.println(thingArray[index]+": I am very fat.");
			}
			if(thingArray[index] == "Locomotive")
			{
				System.out.println(thingArray[index]+": I am mutable.");
			}
			if(thingArray[index] == "Tick-Tock the Crocodile")
			{
				System.out.println(thingArray[index]+": I live in water and on land.");
			}
		}
		System.out.println();
		System.out.println("Creatures:");
		System.out.println();

		for(int index = 4;index < 8;index++)
		{
			System.out.println(thingArray[index]);
		}
	}
}

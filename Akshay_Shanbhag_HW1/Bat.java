
public class Bat extends Creature implements Flyer {

	public static String Name = "";
	Bat(String name) {
		super(name);
		Bat.Name = name;
	}
	
	public void fly() {
		String className = getClass().getSimpleName();
		System.out.println(Name+ " " +className+" Bat is swooping through the dark.");
	}
	public void move() {
		
		fly();
	}
	
	public void eat(Thing aThing)
	{
		String className = getClass().getSimpleName();
		System.out.println(Name + " "+className+" wont eat a "+ aThing);
	}

	
	public void whatDidYouEat()
	{
		String className = getClass().getSimpleName();
		System.out.println(Name+" "+className+" has just eaten some ants.");
	}
}

import static org.junit.Assert.*;

import org.junit.Test;
public class TestCreatureJUnit {

	//move method starts
	@Test
	public void moveTiger() {

		Creature t1 = new Tiger("BengalTiger");
		String className = t1.getClass().getSimpleName();
		t1.move();
		assertEquals(t1.getThing_Name()+" "+className+" has just pounced","BengalTiger Tiger has just pounced");
	}
	@Test
	public void moveAnt() {

		Creature a1 = new Ant("RedAnt");
		String className = a1.getClass().getSimpleName();
		a1.move();
		assertEquals(a1.getThing_Name()+" "+className+" is crawling around.","RedAnt Ant is crawling around.");
	}
	@Test
	public void moveBat() {

		Creature b1 = new Bat("Batman");
		String className = b1.getClass().getSimpleName();
		b1.move();
		assertEquals(b1.getThing_Name()+" "+className+" is swooping through the dark.","Batman Bat is swooping through the dark.");
	}
	@Test
	public void moveFly() {

		Creature f1 = new Fly("ZenFly");
		String className = f1.getClass().getSimpleName();
		f1.move();
		assertEquals(f1.getThing_Name()+" "+className+" is buzzing around in flight.","ZenFly Fly is buzzing around in flight.");
	}

	//move method ends

	
	//Eat method starts
	@Test
	public void eatFly() {

		Creature f1 = new Fly("ZenFly"); 
		Thing t1 = new Thing("Banana");
		String className = f1.getClass().getSimpleName();
		f1.eat(t1);
		assertEquals(f1.getThing_Name()+" "+className+" eats a "+t1.getThing_Name(),"ZenFly Fly eats a Banana");
	}
	
	@Test
	public void wonteatFly() {

		Creature f1 = new Fly("ZenFly"); 
		Tiger t1 = new Tiger("SnowTiger");
		String className = f1.getClass().getSimpleName();
		f1.eat(t1);
		assertEquals(f1.getThing_Name()+" "+className+" won't eat a "+t1.getThing_Name(),"ZenFly Fly won't eat a SnowTiger");
	}


	@Test
	public void eatTiger() {

		Creature tig1 = new Tiger("BengalTiger"); 
		Thing t1 = new Thing("Meat");
		tig1.eat(t1);
		assertEquals(tig1.getThing_Name()+" has just eaten a "+t1.getThing_Name(),"BengalTiger has just eaten a Meat");
	}


	@Test
	public void eatBat() {

		Creature b1 = new Bat("Batman"); 
		Thing t1 = new Thing("leaf");
		String className = b1.getClass().getSimpleName();
		b1.eat(t1);
		assertEquals(b1.getThing_Name()+" "+ className+" wont eat a "+t1.getThing_Name(),"Batman Bat wont eat a leaf");
	}


	@Test
	public void eatAnt() {

		Creature a1 = new Ant("RedAnt"); 
		Thing t1 = new Thing("sugar");
		a1.eat(t1);
		assertEquals(a1.getThing_Name()+" has just eaten a "+t1.getThing_Name()+".","RedAnt has just eaten a sugar.");
	}

	
	//eat method ends
	
	//what did you eat method starts
	@Test
	public void whatDidYouEatFly() {

		Creature a1 = new Fly("ZenFly"); 
		String className = a1.getClass().getSimpleName();
		a1.whatDidYouEat();
		assertEquals(a1.getThing_Name()+" "+className+" has eaten a garbage.","ZenFly Fly has eaten a garbage.");
	}

	@Test
	public void whatDidYouEatBat() {

		Creature a1 = new Bat("Batman"); 
		String className = a1.getClass().getSimpleName();
		a1.whatDidYouEat();
		assertEquals(a1.getThing_Name()+" "+className+" has eaten ants.","Batman Bat has eaten ants.");
	}

	@Test
	public void whatDidYouEatAnt() {

		Creature a1 = new Ant("RedAnt");
		String className = a1.getClass().getSimpleName();
		a1.whatDidYouEat();
		assertEquals(a1.getThing_Name()+" "+className+" has eaten some sugar.","RedAnt Ant has eaten some sugar.");
	}

	@Test
	public void whatDidYouEatTiger() {

		Creature a1 = new Tiger("BengalTiger"); 
		String className = a1.getClass().getSimpleName();
		a1.whatDidYouEat();
		
		assertEquals(a1.getThing_Name()+" "+className+" has had nothing to eat.","BengalTiger Tiger has had nothing to eat.");
	}

	//whatdidyoueat method ends
	
	//fly method starts
	@Test
	public void flyBat() {

		Bat a1 = new Bat("Batman"); 
		String className = a1.getClass().getSimpleName();
		a1.fly();
		assertEquals(a1.getThing_Name()+" "+className+" is swooping through the dark.","Batman Bat is swooping through the dark.");
	}

	@Test
	public void flyFly() {

		Fly a1 = new Fly("ZenFly"); 
		String className = a1.getClass().getSimpleName();
		a1.fly();
		assertEquals(a1.getThing_Name()+" "+className+" is buzzing around in flight.","ZenFly Fly is buzzing around in flight.");
	}

//fly method ends
	
	//tostring method starts
	@Test
	public void toStringTiger() {

		Thing a1 = new Tiger("SnowTiger"); 
		String actual = a1.toString();
		assertEquals(actual,"SnowTiger Tiger");
	}

	@Test
	public void toStringAnt() {

		Thing a1 = new Ant("RedAnt");  
		String actual = a1.toString();
		assertEquals(actual,"RedAnt Ant");
	}

	@Test
	public void toStringFly() {

		Thing a1 = new Fly("ZenFly"); 
		String actual = a1.toString();
		assertEquals(actual,"ZenFly Fly");
	}

	@Test
	public void toStringBat() {

		Thing a1 = new Bat("Batman"); 
		String actual = a1.toString();
		assertEquals(actual,"Batman Bat");
	}
	
	@Test
	public void toStringBox() {

		Thing a1 = new Thing("Box"); 
		String actual = a1.toString();
		assertEquals(actual,"Box");
	}

	//tostring method ends
}

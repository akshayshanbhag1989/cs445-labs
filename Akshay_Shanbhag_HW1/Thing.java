
public class Thing {

	private String Thing_Name;

	public Thing(String name)
	{
		this.setThing_Name(name);
	}


	public String toString()
	{
		String className = getClass().getSimpleName();

		if(className.equals("Thing"))
		{
			return Thing_Name;
		}
		else
		{
			return Thing_Name+ " "+className;
		}
	}

	public String getThing_Name() {
		return Thing_Name;
	}


	public void setThing_Name(String thing_Name) {
		Thing_Name = thing_Name;
	}

}


public class Tiger extends Creature {

	public static String Name;
	
	Tiger( String name)
	{
		super(name);
		Tiger.Name = name;
	}
	
	public void move()
	{
		String className = getClass().getSimpleName();
		System.out.println(Name+" "+className+ " has just pounced");
	}
	
	
	public void whatDidYouEat()
	{
		System.out.println(Name+" Creature has just feasted on meat.");
	}
	
	
	public void eat(Thing aThing)
	{
		System.out.println(Name+" has just eaten a "+aThing);
		
	}

	
}


public abstract class Creature extends Thing {

	public String Name;
	Creature(String name)
	{
		super(name);
		this.Name = name;
	}

	public void eat(Thing aThing)
	{
		System.out.println(Name+" has just eaten a "+aThing);
		   
	}

	public abstract void move();


	public void whatDidYouEat()
	{
		String className = getClass().getSimpleName();
		System.out.println(Name+ " " +className +"has just eaten a sugar.");
	}

}

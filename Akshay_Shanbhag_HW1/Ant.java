
public class Ant extends Creature{

	public static String Name;
	
	Ant(String name) {
		
		super(name);
		Ant.Name = name;
	
	}
	
	public void move()
	{
		String className = getClass().getSimpleName();
		System.out.println(Name+" "+className+" Ant is crawling around.");
	}

	public void whatDidYouEat()
	{
		System.out.println(Name+" Creature has just eaten some sugar.");
	}
	
	public void eat(Thing aThing)
	{
		System.out.println(Name+" has just eaten a "+aThing);
	}
	
}


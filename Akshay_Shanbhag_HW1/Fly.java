
public class Fly extends Creature implements Flyer{

	public static String Name;
	
	Fly(String name) {
		super(name);
		Fly.Name = name;
	}

	
	public void fly() {
		String className = getClass().getSimpleName();
		System.out.println(Name+ " "+className+" is buzzing around in flight.");
	}

	
	public void move() {
			
		fly();
	}

	
	public void eat(Thing aThing)
	{
		String className = getClass().getSimpleName();
		if(className == "Thing")
		{
		System.out.println(Name +" "+className+ "eats a "+ aThing);
		}
		else
		{
			System.out.println(Name +" "+className+ "won't eat a "+ aThing);
		}
	}
	
	
	public void whatDidYouEat()
	{
		System.out.println(Name+" Creature has just eaten some garbage.");
	}
}
